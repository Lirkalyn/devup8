<?php $errorMessage = null;
	if($_POST['monNom'] AND $_POST['monScore']){// teste si le formulaire est renseigné.
		/*<p><?php echo $errorMessage?></p>*/
		$nom = trim($_POST['monNom']);
		$score = $_POST['monScore'];
		$date = time();
		$monFichier = fopen('score.txt', 'a+');// a+ cree le fichier et garde le curseur au bon endroit
		$monTab = array($nom, $date, $score);// création tableau avec les 3 variable.
		$ligne = inplode(':', $monTab);// création d'une ligne séparer par : entre chaque variable du tableau.
		fwrite($monFichier, $ligne."\r\n");
		fclose($monFichier);
	}
	else{
		$errorMessage = 'votre nom est obligatoire';
	}
?>


<!Doctype html>
<html lang = "fr">
<head>
    <title> Jeu UP8 </title> 
    <meta charset = 'UTF-8'>

    <link rel='stylesheet' type = 'text/css' href = 'style.css'>
    <script type = 'text/javascript' src = 'javascript.js'> </script>
    <script> document.addEventListener("DOMContentLoaded", function(event){
        initGame();
        });
    </script> 
</head>
<body>
    <header> 
        <nav>
            <ul> 
                <li> <a href = 'index.html'> Jeu </a> </li>
                <li> <a href = 'scores.html'> Score </a> </li> 
            </ul>    
         </nav>
    </header> 
    
    <main> 
        <div id = 'jeu'> 
            <table class = 'cartes'>
                <caption> Score : <span id = 'score'> 0 </span> </caption>
                <tbody id = 'tabBody'> 
                    <tr> 
                        <td> 
                            <img id = '1' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                         <td> 
                            <img id = '2' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>   
                        
                         <td> 
                            <img id = '3' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>   
                        
                        <td> 
                            <img id = '4' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '5' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '6' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '7' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '8' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '9' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '10' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td> 
                      </tr>  
                       
                      
                       <tr> 
                        <td> 
                            <img id = '11' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                         <td> 
                            <img id = '12' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>   
                        
                         <td> 
                            <img id = '13' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>   
                        
                        <td> 
                            <img id = '14' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '15' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '16' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '17' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '18' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '19' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '20' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td> 
                      </tr>    
                      
                       <tr> 
                        <td> 
                            <img id = '21' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                         <td> 
                            <img id = '22' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>   
                        
                         <td> 
                            <img id = '23' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>   
                        
                        <td> 
                            <img id = '24' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '25' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '26' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '27' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '28' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '29' onClick ="tourne(this)"  src = 'images/back.jpg'>
                        </td>    
                        
                        <td> 
                            <img id = '30' onClick ="tourne(this)" src = 'images/back.jpg'>
                        </td> 
                      </tr>              
                         
                </tbody>    
            </table>
        </div>
        <div id='resultat' class='resultat'>
            <p> Partie terminée </p>
            <p> Votre résultat est <span id = 'score2'> </span> </p> 
            <p id='errorMessage' class = 'errorMessage'> </p>
            <p> <button> REJOUER </button> </p>
            <form name = 'formulaire' id='formumaire' action='' method = 'POST'>
                <input type = 'text' id = 'monNom' name = 'monNom' placeholder='Saisissez votre nom'>
                <input type = 'hidden' id = 'monScore' naùe = 'monScore' value=''> 
            </form>
            <p> 
                <button onClick = 'rejouer();'>  REJOUER </button> </p>
                <button onClick = 'setscore();'> Enregistrer mon score </button> 
            </p>            
            
        </div>     
    </main>     
        
